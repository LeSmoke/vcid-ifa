-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: flask
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Andorra'),(2,'United Arab Emirates'),(3,'Afghanistan'),(4,'Antigua and Barbuda'),(5,'Anguilla'),(6,'Albania'),(7,'Armenia'),(8,'Netherlands Antilles'),(9,'Angola'),(10,'Antarctica'),(11,'Argentina'),(12,'American Samoa'),(13,'Austria'),(14,'Australia'),(15,'Aruba'),(16,'Azerbaijan'),(17,'Bosnia and Herzegovina'),(18,'Barbados'),(19,'Bangladesh'),(20,'Belgium'),(21,'Burkina Faso'),(22,'Bulgaria'),(23,'Bahrain'),(24,'Burundi'),(25,'Benin'),(26,'Bermuda'),(27,'Brunei'),(28,'Bolivia'),(29,'Brazil'),(30,'Bahamas'),(31,'Bhutan'),(32,'Bouvet Island'),(33,'Botswana'),(34,'Belarus'),(35,'Belize'),(36,'Canada'),(37,'Cocos [Keeling] Islands'),(38,'Congo [DRC]'),(39,'Central African Republic'),(40,'Congo [Republic]'),(41,'Switzerland'),(42,'Côte d\'Ivoire'),(43,'Cook Islands'),(44,'Chile'),(45,'Cameroon'),(46,'China'),(47,'Colombia'),(48,'Costa Rica'),(49,'Cuba'),(50,'Cape Verde'),(51,'Christmas Island'),(52,'Cyprus'),(53,'Czech Republic'),(54,'Germany'),(55,'Djibouti'),(56,'Denmark'),(57,'Dominica'),(58,'Dominican Republic'),(59,'Algeria'),(60,'Ecuador'),(61,'Estonia'),(62,'Egypt'),(63,'Western Sahara'),(64,'Eritrea'),(65,'Spain'),(66,'Ethiopia'),(67,'Finland'),(68,'Fiji'),(69,'Falkland Islands [Islas Malvinas]'),(70,'Micronesia'),(71,'Faroe Islands'),(72,'France'),(73,'Gabon'),(74,'United Kingdom'),(75,'Grenada'),(76,'Georgia'),(77,'French Guiana'),(78,'Guernsey'),(79,'Ghana'),(80,'Gibraltar'),(81,'Greenland'),(82,'Gambia'),(83,'Guinea'),(84,'Guadeloupe'),(85,'Equatorial Guinea'),(86,'Greece'),(87,'South Georgia and the South Sandwich Islands'),(88,'Guatemala'),(89,'Guam'),(90,'Guinea-Bissau'),(91,'Guyana'),(92,'Gaza Strip'),(93,'Hong Kong'),(94,'Heard Island and McDonald Islands'),(95,'Honduras'),(96,'Croatia'),(97,'Haiti'),(98,'Hungary'),(99,'Indonesia'),(100,'Ireland'),(101,'Israel'),(102,'Isle of Man'),(103,'India'),(104,'British Indian Ocean Territory'),(105,'Iraq'),(106,'Iran'),(107,'Iceland'),(108,'Italy'),(109,'Jersey'),(110,'Jamaica'),(111,'Jordan'),(112,'Japan'),(113,'Kenya'),(114,'Kyrgyzstan'),(115,'Cambodia'),(116,'Kiribati'),(117,'Comoros'),(118,'Saint Kitts and Nevis'),(119,'North Korea'),(120,'South Korea'),(121,'Kuwait'),(122,'Cayman Islands'),(123,'Kazakhstan'),(124,'Laos'),(125,'Lebanon'),(126,'Saint Lucia'),(127,'Liechtenstein'),(128,'Sri Lanka'),(129,'Liberia'),(130,'Lesotho'),(131,'Lithuania'),(132,'Luxembourg'),(133,'Latvia'),(134,'Libya'),(135,'Morocco'),(136,'Monaco'),(137,'Moldova'),(138,'Montenegro'),(139,'Madagascar'),(140,'Marshall Islands'),(141,'Macedonia [FYROM]'),(142,'Mali'),(143,'Myanmar [Burma]'),(144,'Mongolia'),(145,'Macau'),(146,'Northern Mariana Islands'),(147,'Martinique'),(148,'Mauritania'),(149,'Montserrat'),(150,'Malta'),(151,'Mauritius'),(152,'Maldives'),(153,'Malawi'),(154,'Mexico'),(155,'Malaysia'),(156,'Mozambique'),(157,'Namibia'),(158,'New Caledonia'),(159,'Niger'),(160,'Norfolk Island'),(161,'Nigeria'),(162,'Nicaragua'),(163,'Netherlands'),(164,'Norway'),(165,'Nepal'),(166,'Nauru'),(167,'Niue'),(168,'New Zealand'),(169,'Oman'),(170,'Panama'),(171,'Peru'),(172,'French Polynesia'),(173,'Papua New Guinea'),(174,'Philippines'),(175,'Pakistan'),(176,'Poland'),(177,'Saint Pierre and Miquelon'),(178,'Pitcairn Islands'),(179,'Puerto Rico'),(180,'Palestinian Territories'),(181,'Portugal'),(182,'Palau'),(183,'Paraguay'),(184,'Qatar'),(185,'Réunion'),(186,'Romania'),(187,'Serbia'),(188,'Russia'),(189,'Rwanda'),(190,'Saudi Arabia'),(191,'Solomon Islands'),(192,'Seychelles'),(193,'Sudan'),(194,'Sweden'),(195,'Singapore'),(196,'Saint Helena'),(197,'Slovenia'),(198,'Svalbard and Jan Mayen'),(199,'Slovakia'),(200,'Sierra Leone'),(201,'San Marino'),(202,'Senegal'),(203,'Somalia'),(204,'Suriname'),(205,'São Tomé and Príncipe'),(206,'El Salvador'),(207,'Syria'),(208,'Swaziland'),(209,'Turks and Caicos Islands'),(210,'Chad'),(211,'French Southern Territories'),(212,'Togo'),(213,'Thailand'),(214,'Tajikistan'),(215,'Tokelau'),(216,'Timor-Leste'),(217,'Turkmenistan'),(218,'Tunisia'),(219,'Tonga'),(220,'Turkey'),(221,'Trinidad and Tobago'),(222,'Tuvalu'),(223,'Taiwan'),(224,'Tanzania'),(225,'Ukraine'),(226,'Uganda'),(227,'U.S. Minor Outlying Islands'),(228,'United States'),(229,'Uruguay'),(230,'Uzbekistan'),(231,'Vatican City'),(232,'Saint Vincent and the Grenadines'),(233,'Venezuela'),(234,'British Virgin Islands'),(235,'U.S. Virgin Islands'),(236,'Vietnam'),(237,'Vanuatu'),(238,'Wallis and Futuna'),(239,'Samoa'),(240,'Kosovo'),(241,'Yemen'),(242,'Mayotte'),(243,'South Africa'),(244,'Zambia'),(245,'Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gender` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gender`
--

LOCK TABLES `gender` WRITE;
/*!40000 ALTER TABLE `gender` DISABLE KEYS */;
INSERT INTO `gender` VALUES (1,'Male'),(2,'Female');
/*!40000 ALTER TABLE `gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `zip` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `age` int NOT NULL,
  `country_idcountry` int NOT NULL,
  `gender_idgender` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_person_country_idx` (`country_idcountry`),
  KEY `fk_person_gender1_idx` (`gender_idgender`),
  CONSTRAINT `fk_person_country` FOREIGN KEY (`country_idcountry`) REFERENCES `country` (`id`),
  CONSTRAINT `fk_person_gender1` FOREIGN KEY (`gender_idgender`) REFERENCES `gender` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-27 12:30:50

from flask import Flask, render_template, request, redirect, jsonify
from flask_login import login_user, logout_user, current_user
from flask_sqlalchemy import SQLAlchemy
import flask_login
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@mysql/flask'
app.secret_key = 'ifa2022'
db = SQLAlchemy(app)

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

if __name__ == "__main__":
    app.run(debug=True, port=80)


class Gender(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    gender_idgender = db.relationship('Person', backref='gender', lazy=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Gender %r>' % self.id


class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    country_idcountry = db.relationship('Person', backref='country', lazy=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Country %r>' % self.id


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(255), nullable=False)
    lastname = db.Column(db.String(255), nullable=False)
    phone = db.Column(db.String(255), nullable=False)
    street = db.Column(db.String(255), nullable=False)
    city = db.Column(db.String(255), nullable=False)
    zip = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)
    age = db.Column(db.Integer, nullable=False)
    country_idcountry = db.Column(db.Integer, db.ForeignKey('country.id'), nullable=False)
    gender_idgender = db.Column(db.Integer, db.ForeignKey('gender.id'), nullable=False)

    def __init__(self, firstname, lastname, phone, street, city, zip, email, age, country_idcountry, gender_idgender):
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.street = street
        self.city = city
        self.zip = zip
        self.email = email
        self.age = age
        self.country_idcountry = country_idcountry
        self.gender_idgender = gender_idgender

    def __repr__(self):
        return '<Person %r>' % self.id


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False)

    def __init__(self, name, password, email):
        self.name = name
        self.password = generate_password_hash(password)
        self.email = email

    def __repr__(self):
        return f'<User {self.username}>'

    def verify_password(self, pwd):
        return check_password_hash(self.password, pwd)

    def to_json(self):
        return {"name": self.name,
                "email": self.email}

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)


@login_manager.user_loader
def load_user(id):
    user = User.query.get(id)
    return User(user.name, user.password, user.email)


@app.route('/register', methods=['POST', 'GET'])
def register():
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']
        email = request.form['email']

        new_user = User(name=name, password=password, email=email)

        try:
            db.session.add(new_user)
            db.session.commit()
            return redirect('/login')
        except Exception as e:
            print(e)
            print("starting here")
            print(new_user.password)
            print("ending here")
            return "There was an issue with registering your user"
    else:
        return render_template('register.html')


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        users = User.query.order_by(User.id).all()
        for user in users:
            if email == user.email and check_password_hash(user.password, password):
                login_user(user)
                return redirect("/")
        return render_template('login.html', failure=True)
    else:
        return render_template('login.html', failure=False)


@app.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect("/login")


@app.route('/user_info', methods=['POST'])
def user_info():
    if current_user.is_authenticated:
        resp = {"result": 200,
                "data": current_user.to_json()}
    else:
        resp = {"result": 401,
                "data": {"message": "user no login"}}
    return jsonify(**resp)


@app.route('/', methods=['POST', 'GET'])
def index():
    if not current_user.is_authenticated:
        return redirect('/login')

    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        phone = request.form['phone']
        street = request.form['street']
        city = request.form['city']
        zip = request.form['zip']
        email = request.form['email']
        age = request.form['age']
        country_idcountry = request.form['country_idcountry']
        gender_idgender = request.form['gender_idgender']

        new_person = Person(firstname=firstname, lastname=lastname, phone=phone, street=street, city=city, zip=zip,
                            email=email,
                            age=age, country_idcountry=country_idcountry, gender_idgender=gender_idgender)

        try:
            db.session.add(new_person)
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue adding your Person'

    else:
        persons = Person.query.order_by(Person.id).all()
        countries = Country.query.order_by(Country.id).all()
        genders = Gender.query.order_by(Gender.id).all()
        return render_template('index.html', persons=persons, countries=countries, genders=genders)


@app.route('/delete/<int:id>')
def delete(id):
    person_to_delete = Person.query.get_or_404(id)

    try:
        db.session.delete(person_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'There was a problem deleting that Person'


@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    person = Person.query.get_or_404(id)
    countries = Country.query.order_by(Country.id).all()
    genders = Gender.query.order_by(Gender.id).all()

    if request.method == 'POST':
        person.firstname = request.form['firstname']
        person.lastname = request.form['lastname']
        person.phone = request.form['phone']
        person.street = request.form['street']
        person.city = request.form['city']
        person.zip = request.form['zip']
        person.email = request.form['email']
        person.age = request.form['age']
        person.country_idcountry = request.form['country_idcountry']
        person.gender_idgender = request.form['gender_idgender']

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'There was an issue updating your person'

    else:
        return render_template('update.html', person=person, countries=countries, genders=genders)

document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("search").addEventListener("keyup", function(event) {
      event.preventDefault();
      let input = this.value.toLowerCase();
      Array.prototype.filter.call(document.querySelectorAll("#table tr"), function(tr, index) {
        if (index === 0) {
          return true;
        }
        if (tr.innerText.toLowerCase().indexOf(input) > -1) {
          tr.style.display = "";
        } else {
          tr.style.display = "none";
        }
      });
    });
});

FROM mysql:latest
ENV MYSQL_DATABASE=root \
    MYSQL_ROOT_PASSWORD=root \
    MYSQL_USER=app \
    MYSQL_PASSWORD=123456

ADD db/init.sql /docker-entrypoint-initdb.d

FROM tiangolo/meinheld-gunicorn-flask:python3.7
WORKDIR /app
COPY . .
#RUN apt-get update
#RUN apt-get -y install python3-pip
#RUN apt-get -y install default-libmysqlclient-dev
RUN pip install virtualenv
RUN virtualenv env
RUN ./env/Scripts/activate
RUN pip install -r requirements.txt
#RUN mysql -u app -p123456 flask < init.sql
#RUN python main.py

ENV PORT=80
EXPOSE 80
ENTRYPOINT ["./gunicorn.sh"]
#CMD ["gunicorn", "--bind", "0.0.0.0:80", "app.main:app"]
#CMD ["python", "main.py"]
